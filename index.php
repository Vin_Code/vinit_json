<?php
/**
 * Created by PhpStorm.
 * User: vinitpayal
 * Date: 23/12/15
 * Time: 12:38 PM
 */

?>
<html>
    <title>Enter Required Info</title>
    <head>
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <script src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.11.3.min.js"></script>
    </head>

    <body>

    <form method="post" action="json_decode.php" style="padding-top: 15px">

        <table class="table-striped" width="60%" align="center" cellspacing="4px">
            <tr>
                <td colspan="2" style="padding-left: 35%;color:rebeccapurple;font-family: cursive;font-weight: bolder;font-size: large">JSON Parser</td>
            </tr>
            <tr style="display: none;">
                <td>Enter <b>HOST</b> for DB</td>
                <td>
                    <input type="text" name="db_host" class="form-control" id="db_host">
                </td>
            </tr>

            <tr style="display: none;">
                <td>Enter DB User Name</td>
                <td>
                    <input type="text" name="db_user_name" class="form-control" id="db_user_name">
                </td>
            </tr>
            <tr style="display: none;">
                <td>Enter DB Password</td>
                <td>
                    <input type="password" id="db_pswd" name="db_pswd" class="form-control">
                </td>
            </tr>
            <tr style="display: none;" class="change_display_after_validation" >
                <td>Enter The DB name:</td>
                <td>
                    <input type="text" name="db_name" class="form-control">
                </td>
            </tr>
            <tr class="change_display_after_validation" >
                <td>Enter Table Name</td>
                <td>
                    <input type="text" class="form-control" name="table_name">
                </td>
            </tr>
            <tr class="change_display_after_validation" >
                <td>Coloumn Name which  contains JSON</td>
                <td>
                    <input type="text" name="json_col_name" class="form-control">
                </td>
            </tr>
            <tr class="change_display_after_validation" >
                <td>Fields names to extract from JSON </td>
                <td>
                    <input type="text" name="json_fields_name" class="form-control">
                </td>
                <td style="font-size: small">*Enter all fields in comma seperated form</td>
            </tr>
            <tr>
                <td>Date After</td>
                <td>
                    <input type="text" name="date_after" class="form-control"> </input>
                </td>
            </tr>
            <tr>
                <td>Date before</td>
                <td>
                    <input type="text" name="date_before" class="form-control"> </input>
                </td>
            </tr>

            <tr>
              <td>Date field if any</td>
              <td>
                  <input type="text" name="date_coloumn" class="form-control">
              </td>
            </tr>
            <tr class="change_display_after_validation" >
                <td>Enter Unique column to display</td>
                <td>
                    <input type="text" name="unique_col" class="form-control">
                </td>
            </tr>
            <tr class="change_display_after_validation"  >
                <td colspan="2" align="right" style="padding-top: 6px">
                    <input type="submit" class="btn btn-default" value="Submit">
                </td>
            </tr>

        </table>
    </form>

    <script type="text/javascript">
        function checkDbAuthentication()
        {
            var db_host=document.getElementById("db_host").value;
            var db_user_name=document.getElementById("db_user_name").value;
            var db_pswd=document.getElementById("db_pswd").value;
            $.ajax({
                url: "validate_db.php",
                type:'POST',
                data:{
                    'db_host':db_host,
                    'db_user_name':db_user_name,
                    'db_pswd':db_pswd
                }
            }).done(function(response) {
                var x=document.getElementsByClassName('change_display_after_validation');
                var i;
                if(response=="true")
                {
                    //var x=document.getElementsByClassName('change_display_after_validation');
                    //var i;
                    for (i = 0; i < x.length; i++) {
                        x[i].style.display ="table-row";
                    }
                }
                else
                {
                    for (i = 0; i < x.length; i++) {
                        x[i].style.display ="none";
                    }
                    //location.reload(false);

                    alert('Wrong Details Entered');
                }

            });
        }
    </script>

    </body>
</html>
